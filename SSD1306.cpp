#include "SSD1306.h"

#include <Wire.h>

#if defined(BUFFER_LENGTH)
	#define WIRE_MAX BUFFER_LENGTH          ///< AVR or similar Wire lib
#elif defined(SERIAL_BUFFER_SIZE)
	#define WIRE_MAX (SERIAL_BUFFER_SIZE-1) ///< Newer Wire uses RingBuffer
#else
	#define WIRE_MAX 32                     ///< Use common Arduino core default
#endif

SSD1306::SSD1306(
	uint8_t i2cAddress, 
	size_t width, 
	size_t height
) {
	this->i2cAddress = i2cAddress;
	this->width = width;
	this->height = height;
	rowsCount = (height + 7) / 8;
	bufferSize = width*rowsCount;
	buffer = (uint8_t*)malloc(bufferSize);
	for (size_t i = 0; i < bufferSize; i++) {
		buffer[i] = 0;
	}
	displayWasInitialized = false;
	hasChangesToCommit = true;
}

SSD1306::~SSD1306() {
	free(buffer);
}
    
bool SSD1306::RGBAToColor(RGBA rgba) {
	return (RGBAs::extractRed(rgba) + RGBAs::extractGreen(rgba) + RGBAs::extractBlue(rgba) > 127 * 3);
}

void SSD1306::sendBytes(
	uint8_t mode,
	std::function<void(std::function<void(uint8_t)>)> provideBytes
) {
	Wire.beginTransmission(i2cAddress);
    Wire.write(mode);
    uint8_t sentBytes = 1;
    provideBytes(
		[&](uint8_t byteToSend) { 
			if (sentBytes >= WIRE_MAX) {
				Wire.endTransmission();
				Wire.beginTransmission(i2cAddress);
				Wire.write(mode);
				sentBytes = 1;
			}
			Wire.write(byteToSend);
			sentBytes++;
		}
    );
    Wire.endTransmission();
}

void SSD1306::initializeDisplayIfNeed() {
	if (displayWasInitialized) {
		return;
	}
	sendBytes(
		modeCommand, 
		[&](std::function<void(uint8_t)> bytesReceiver) { 
			bytesReceiver(0xAE);//выключение дисплея
			bytesReceiver(0xD5);
			bytesReceiver(0x80);
			bytesReceiver(0xD3);
			bytesReceiver(0x0);
			bytesReceiver(0x40);
			bytesReceiver(0x8D);//включение емкостного умножителя
			bytesReceiver(0x14);
			bytesReceiver(0x20);//настройка адресации
			bytesReceiver(0x00);// 0х00 для горизонтальной, 0х01 для вертикальной, 0х02 для постраничной адресации
			bytesReceiver(0xA1);//отражение по горизонтали, для отображения справа налево необходимо использовать команду 0xA0
			bytesReceiver(0xC8);//отражение по вертикали, 0xC0 для переворота изображения по вертикали.
			bytesReceiver(0xDA);
			bytesReceiver(0x12);
			bytesReceiver(0x81);//установка контрастности дисплея
			bytesReceiver(0xCF);
			bytesReceiver(0xD9);
			bytesReceiver(0xF1);
			bytesReceiver(0xDB);
			bytesReceiver(0x40);
			bytesReceiver(0xA4);
			bytesReceiver(0xA6);//инверсия дисплея, 0xA6 для отключения инверсии, 0xA7 для включения инверсии цвета.
			bytesReceiver(0xAF);//включение дисплея
		}
	);
	displayWasInitialized = true;
}

size_t SSD1306::getWidth() {
	return width;
}

size_t SSD1306::getHeight() {
	return height;
}

void SSD1306::setPixelColor(size_t x, size_t y, RGBA color) {
	
	if (RGBAs::extractAlpha(color) < 128) {
		return;
	}
	
	if (
		x < 0 || x >= width ||
		y < 0 || y >= height
	) {
		return;
	}
	
	bool newValue = RGBAToColor(color);
	size_t byteIndex = x + (y / 8) * width;
	uint8_t oldByte = buffer[byteIndex];
	uint8_t bitIndex = y % 8;
	bool oldValue = (oldByte >> bitIndex) & 1;
	if (oldValue == newValue) {
		return;
	}
	
	buffer[byteIndex] = oldByte ^ (1 << bitIndex);
	hasChangesToCommit = true;
}

void SSD1306::sendCommandSetColumnsRange(
	std::function<void(uint8_t)> bytesReceiver, 
	size_t first, 
	size_t last
) {
	bytesReceiver(commandSetColumnsRange);
	bytesReceiver(first);
	bytesReceiver(last);
}

void SSD1306::sendCommandSetRowsRange(
	std::function<void(uint8_t)> bytesReceiver, 
	size_t first, 
	size_t last
) {
	bytesReceiver(commandSetRowsRange);
	bytesReceiver(first);
	bytesReceiver(last);
}

void SSD1306::commit() {
	if (!hasChangesToCommit) {
		return;
	}
	initializeDisplayIfNeed();
	sendBytes(
		modeCommand, 
		[&](std::function<void(uint8_t)> bytesReceiver) { 
			sendCommandSetColumnsRange(bytesReceiver, 0, width - 1);
			sendCommandSetRowsRange(bytesReceiver, 0, rowsCount - 1);
		}
	);
	sendBytes(
		modeBytes, 
		[&](std::function<void(uint8_t)> bytesReceiver) { 
			for (size_t i = 0; i < bufferSize; i++) {
				bytesReceiver(buffer[i]);
			}
		}
	);
	hasChangesToCommit = false;
}
