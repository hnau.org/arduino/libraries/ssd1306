#ifndef SSD1306_H
#define SSD1306_H

#include <Canvas.h>

class SSD1306: public Canvas {

private:

    static const uint8_t modeCommand = 0x00;
    static const uint8_t modeBytes = 0x40;
    
    static const uint8_t commandSetColumnsRange = 0x21;
    static const uint8_t commandSetRowsRange = 0x22;
    
    uint8_t i2cAddress;
    size_t width;
    size_t height;
    size_t rowsCount;
    size_t bufferSize;
    
    bool hasChangesToCommit;
    uint8_t* buffer;
    
    bool RGBAToColor(RGBA rgba);
    
    bool displayWasInitialized;
    void initializeDisplayIfNeed();
    
    void sendBytes(uint8_t mode, std::function<void(std::function<void(uint8_t)>)> provideBytes);
    
    void sendCommandSetColumnsRange(std::function<void(uint8_t)> bytesReceiver, size_t first, size_t last);
    void sendCommandSetRowsRange(std::function<void(uint8_t)> bytesReceiver, size_t first, size_t last);
    

public:

	SSD1306(uint8_t i2cAddress, size_t width, size_t height);
	~SSD1306();

	virtual size_t getWidth() override;
	virtual size_t getHeight() override;
	
	virtual void setPixelColor(size_t x, size_t y, RGBA color) override;
	virtual void commit() override;

};


#endif //SSD1306_H

